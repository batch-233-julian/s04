SQL - Advance Selects and Joining Tables


INSERT INTO artists (name)
VALUES ("Taylor Swift"),("Lady Gaga"),("Justin Bieber"),("Ariana Grande"), ("Bruno Mars");

INSERT INTO albums (album_title, date_released, artist_id)
VALUES ("Fearless", "2008-11-11", 5), ("Red", "2012-10-22", 5), ("A Star Is Born", "2018-10-05", 6), ("Born This Way", "2011-05-23", 6), ("Purpose", "2015-11-130", 7), ("Believe", "2012-06-15", 7), ("Dangerous Woman", "2016-05-20", 8), ("Thank U, Next", "2019-02-08", 8), ("24 Magic", "2016-11-18" , 9), ("Earth to Mars", "2011-02-07", 9);

INSERT INTO songs (song_name, lenght, genre, album_id)
VALUES ("Love Story", 335, "Country Pop", 5), ("State of Grace", 455, "Rock, alternative rock, arena rock", 6), ("Red", 341, "Country", 6), ("Black Eyes", 304, "Rock and Roll", 7), ("Shallow", 336, "Country, rock, folk rock", 7 ), ("Born This Way", 420, "Electropop", 8), ("Sorry", 320, "Dancehall-poptropical, housemoombahton", 9), ("Boyfriend", 252, "Pop", 10), ("Into you", 405, "EDM House", 11), ("Thank U, Next", 327, "Pop, R&B", 12 ), ("24k Magic", 346, "Funk, disco, R&B", 13), ("Lost", 321, "Pop", 14);

SELECT * FROM songs WHERE id !=11;
SELECT * FROM songs WHERE album_id != 8 AND album_id != 9;

2. Finding records using comparison operators
	Terminal
	Syntax
		SELECT * FROM songs WHERE lenght > 230;
		SELECT * FROM songs WHERE lenght < 200;
		SELECT * FROM songs WHERE lenght > 230 OR lenght < 200;
		SELECT * FROM songs WHERE genre = "Pop";

3. Getting records with specific conditions
	Terminal
	Syntax
		-can be use for querying multiple columns
		SELECT column_name FROM table_name WHERE condition;
	Example
	SELECT * FROM songs WHERE id = 6 OR id = 7 OR id = 8;

	Syntax
		SELECT column_name FROM table_name WHERE column_name IN (values);
	Example
		SELECT * FROM songs WHERE id IN(6, 7, 8);
		SELECT * FROM songs WHERE genre IN ("Pop", "Electropop", "EDM House");

4. Show records with partial match
	Terminal
		LIKE clause
		Percent(%) symbols and underscore(_) are called wildcard operators
		% -> represents zero or multiple characters

		a. Find a values with a match at the start
		SELECT * FROM songs WHERE song_name LIKE "th%";

		b. Find values with a match at the end
		SELECT * FROM songs WHERE song_name LIKE "%ce";

		c. Find values with a match at any position
		SELECT * FROM songs WHERE song_name LIKE "%or%";

		d. Find values with a match of a specific lenght/patern
		SELECT * FROM songs WHERE song_name LIKE "__rr_";

		e. Find values with a match at certain position
		SELECT * FROM albums WHERE album_title LIKE "_ur%";

5. Sorting records
	Terminal
	Syntax
		SELECT column_name FROM table_name ORDER BY column_name ORDER;
	Example 
		SELECT * FROM songs ORDER BY song_name;
		SELECT * FROM songs ORDER BY song_name ASC;
		SELECT * FROM songs ORDER BY song_name DESC;

		--ASC is for "ascending"
		--DESC is for "descending"

6. Limiting Records
	Terminal
	SELECT * FROM songs LIMIT 5;


7. Showing Records with distinct values
	Terminal 
	SELECT genre FROM songs;
	--using DESTINCT keyword to avoid repetition of values from retrieval
	SELECT DISTINCT genre FROM songs;

8. Joining TWO tables
	Terminal
	Syntax
		SELECT column_name FROM table1
		JOIN table2 ON table1.id = table2.foreign_key_column,
		JOIN table2 ON table1.id = table2.foreign_key_column;

	Example
		SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

9. Joining Tables with specified WHERE condition
	Example
		SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE name LIKE "%a%";

10. Selecting columns to be displayed from joining tables
	Example
		SELECT name, album_title, date_released, song_name, lenght, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

11. Providing aliases for joining table
	Terminal 
	Syntax
		SELECT column_name AS alias FROM table1
		JOIN table2 ON table1.id = table2.foreign_key_column
		JOIN table2 ON table2.id = table3.foreign_key_column
	Example
		SELECT name AS band, album_title AS album, date_released, song_name AS song, lenght, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

12. Displaying data from joining tables
	--create user information, a playlist and a songs added to the user playlist
	INSERT INTO users (username, password, full_name, contact_number, email, address)
	VALUES ("josh", "josh1234", "Josue Julian", 1123456789, "josh@gmail.com", "Pasig City");

	INSERT INTO playlists (user_id, datetime_created) VALUES (1, "2022-09-20 01:00:00");

	INSERT INTO playlists_songs (playlist_id, song_id)
	VALUES (1, 4), (1, 10), (1, 14);

	Joining multiple tables
	SELECT * FROM playlists JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id JOIN songs ON playlists_songs.song_id = songs.id;

	Selecting specific columns to be displayed from the query
	SELECT user_id, datetime_created, song_name, lenght, genre, album_id FROM playlists JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id JOIN songs ON playlists_songs.song_id = songs.id;